#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>

#include "Class.h"
#include "new.h"
#include "Point.h"
#include "String.h"

struct Point {
    const void * class;
    void * station;     // String
    void * arrival;     // String
    void * departure;   // String
};

static void * Point_interview (void * _self, va_list * app)
{
    struct Point * self = _self;
    
    self -> station   = interview(String, "station:", 20);
    self -> arrival   = interview(String, "arrival time:", 20);
    self -> departure = interview(String, "departure time:", 20);

    return self;
}

static void * Point_ctor (void * _self, va_list * app)
{
    struct Point * self = _self;
     
    const char * station   = va_arg(* app, const char *);
    const char * arrival   = va_arg(* app, const char *);
    const char * departure = va_arg(* app, const char *);

    assert(station && departure && arrival);
    self -> station = new(String, station);
    self -> arrival = new(String, arrival);
    self -> departure = new(String, departure);
    
    return self;    
}

static void * Point_dtor (void * _self)
{
    struct Point * self = _self;

    delete(self -> station);
    delete(self -> departure);
    delete(self -> arrival);

    return 0;
}

static void * Point_clone (const void * _self)
{
    const struct Point * self = _self;

    return new(Point, self -> station, self -> departure, self -> arrival);
}

static int Point_differ (const void * _self, const void * _b)
{
    const struct Point * self = _self;
    const struct Point * b = _b;

    if (self == b) 
        return 0;

    if (! b || b -> class != Point) 
        return 1;

    if (! differ(self -> station, b -> station)
    && (! differ(self -> departure, b -> departure))
    && (! differ(self -> arrival, b -> arrival)))
        return 0;
    
    return 1; 
}

static int Point_metric (const void * _self, const void * _b, va_list * app)
{
    /**************************************************************************
     * RETURN VALUE                                                           *
     *     -1 if _self less than _b                                           *
     *      0 if _self equal _b                                               *
     *      1 if _self greater than _b                                        *
     *************************************************************************/
    
    const struct Point * self = _self; 
    const struct Point * b = _b;
    
    // enum pcompration
    int comp = va_arg(* app, int);
    
    if (comp == STAT)
        return metric(self -> station, b -> station);

    if (comp == ARR)
        return metric(self -> arrival, b -> arrival);

    if (comp == DEP)
        return metric(self -> departure, b -> departure);
}

static int Point_similar (const void * _self, const void * _b, va_list * app)
{
    const struct Point * s = _self;
    const struct Point * b = _b;

    int comp = va_arg(* app, int);

    if (comp == STAT)
        return similar(s -> station, b -> station);

    if (comp == ARR)
        return similar(s -> arrival, b -> arrival);

    if (comp == DEP)
        return similar(s -> departure, b -> departure);
}

static void * Point_print (const void * _self)
{
    const struct Point * self = _self;

    printf("%-20s", text(self -> station));
    printf("to at ");
    print(self -> arrival);
    printf(" from at ");
    print(self -> departure);
    
    return (void *) _self;
}

static void * Point_store (FILE * file, const void * _self)
{
    const struct Point * self = _self;
    const struct Class * string = String; 

    string -> store(file, self -> station);
    string -> store(file, self -> arrival);
    string -> store(file, self -> departure);

    return (void *) self;
}
    
static void * Point_extract (FILE * file, const void * _class, va_list * app)
{
    const struct Class * string = String;

    void * s = string -> extract(file, String, app);
    void * a = string -> extract(file, String, app);
    void * d = string -> extract(file, String, app);
    void * self = new(_class, text(s), text(a), text(d));

    delete(s), delete(d), delete(a);

    return self; 
}

static const struct Class _Point = {
    .size = sizeof(struct Point),
    .interview = Point_interview,
    .ctor = Point_ctor,
    .dtor = Point_dtor,
    .clone = Point_clone,
    .differ = Point_differ,
    .metric = Point_metric,
    .similar = Point_similar,
    .print = Point_print,
    .store = Point_store,
    .extract = Point_extract
};

const void * Point = & _Point;

// Point's functions
const char * getStationName (const void * point)
{
    const struct Point * p = point;

    assert(p && p -> station);
    return p -> station;
}

const char * getArrivalTime (const void * point)
{
    const struct Point * p = point;

    assert(p && p -> arrival);
    return p -> arrival;
}

const char * getDepartureTime (const void * point)
{
    const struct Point * p = point;

    assert(p && p -> departure);
    return p -> departure;
}

