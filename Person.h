#ifndef PERSON_H
#define PERSON_H

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "Space.h"
#include "Class.h"
#include "new.h"
#include "String.h"

extern const void * Person;

const char * getPersonName (const void * person);

int getPersonAge (const void * person);

#endif
