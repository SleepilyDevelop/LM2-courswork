#include "Train.h"

// new(Train, <String>, Set<Point>, <Space>);
struct Train {
	const void * class; // Class
    void * name;        // String
    void * route;       // Set<Point>
    void * space;       // Space
};

static void * Train_interview (void * _self, va_list * app)
{
    struct Train * self = _self;

    self -> name  = interview(String, "Train name", 16);
    self -> route = interview(Set, Point);
    self -> space = interview(Space);

    return (void *) self; 
}

static void * Train_ctor (void * _self, va_list * app)
{
	struct Train * self = _self;
    
    self -> name = va_arg(* app, void *);
    self -> route = va_arg(* app, void *);
    self -> space = va_arg(* app, void *);

    assert(self -> route && self -> space && self -> name);

	return self;
}

static void * Train_dtor (void * _self)
{
	struct Train * self = _self;
    delete(self -> route);
    delete(self -> space);
    delete(self -> name);
    
	return 0;
}

static int  Train_metric (const void * _self, const void * _b, va_list * app)
{
    const struct Train * self = _self;
    const struct Train * b = _b;
    
    int comp = va_arg(* app, int);

    if (comp == NAME)
        return metric(self -> name, b -> name);

    if (comp == SPACE)
    {
        int type = va_arg(* app, int);

        return metric(self -> space, b -> space, type);
    }
}

static int Train_similar (const void * _self, const void * _b, va_list * app)
{
    /* _b may be any like type */
    const struct Train * s = _self;

    int comp = va_arg(* app, int);

    if (comp == NAME) // _b is String
       return similar(s -> name, _b);

    if (comp == ROUTE)// _b is Point
    {
        if (sub(s -> route, _b, STAT) == 0)
           return 0;
        else 
           return 1;
    }

    if (comp == SPACE)// _b is Space
        return 0;

    return 0;
}

static void * Train_print (const void * _self)
{
    const struct Train * self = _self;
    const void * first = get(self -> route, 0);
    const void * last  = get(self -> route, power(self -> route) - 1);
    
    printf("%-20s", text(getDepartureTime(first)) );

    printf("%-20s", text(getStationName(first)) );
    printf("%-20s", text(getStationName(last)) );
    printf("%-16s", text(getArrivalTime(last)) );

    return (void *) _self;
}

static void * Train_store (FILE * file, const void * _self)
{
    const struct Train * self = _self;
    const struct Class * route = Set;
    const struct Class * space = Space;
    const struct Class * name = String;

    name -> store(file, self -> name); 
    route -> store(file, self -> route);
    space -> store(file, self -> space);
	
    return (void *) _self;
} 

static void * Train_extract (FILE * file, const void * _class, va_list * app)
{
	void * n = extract(file, String);
	void * r = extract(file, Set, Point);
	void * s = extract(file, Space);

    return new(_class, n, r, s);
}

static const struct Class _Train = {
    .size = sizeof(struct Train),
    .interview = Train_interview,
    .ctor = Train_ctor,
    .dtor = Train_dtor,
    .metric = Train_metric,
    .similar = Train_similar,
    .print = Train_print,
    .store = Train_store,
    .extract = Train_extract
};

const void * Train = & _Train;

// Train's functions
void * getTrainRoute (const void * _train)
{
    const struct Train * train = _train;

    assert(train &&  train -> route);
    return train -> route;
}

void * getTrainSpace (const void * _train)
{
    const struct Train * train = _train;

    assert(train && train -> space);
    return train -> space;
}

void * getTrainName (const void * _train)
{
    const struct Train * train = _train;

    assert(train && train -> name);
    return train -> name;
}

void * printTrainDetails (const void * _train)
{
    const struct Train * train = _train;

    print(getTrainName(_train));
    putchar('\t');
    printf("%s: %d | ", "Platzcard", getFreeSpace( getTrainSpace(_train), HARD));
    printf("%s: %d | ", "Coupe", getFreeSpace( getTrainSpace(_train), MIDL));
    printf("%s: %d \n", "SW", getFreeSpace( getTrainSpace(_train), EASY));

    for (int i = 0; i < power( getTrainRoute(_train)); ++ i)
        printf("\t-->\t"), print(get(getTrainRoute(_train), i)), putchar('\n');
}

void * reserve (const void * _train) 
{
    const struct Train * train = _train;

    void * sp = getTrainSpace(_train);
    void * trname = clone(train -> name);
    void * person = interview(Person);

    int type = 0;
    printf("%d %-20s\n", getFreeSpace(sp, 0), "[0] Standart");
    printf("%d %-20s\n", getFreeSpace(sp, 1), "[1] Standart prem.");
    printf("%d %-20s\n", getFreeSpace(sp, 2), "[2] Buisness prem.");
    printf("%-20s: ", "Kind of ticket");

    scanf("%d", & type); getchar();

    CLEAR_SCRIN;

	return new(Booking, person, trname, lower(sp, type));
}

