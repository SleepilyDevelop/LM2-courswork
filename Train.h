#ifndef TRAIN_H
#define TRAIN_H

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>

#include "macro.h"
#include "file.h"
#include "new.h"
#include "Class.h"
#include "Set.h"
#include "String.h"
#include "Space.h"
#include "Point.h"
#include "Booking.h"


/**************************************
 * new(Train, name, route, space);
 * extract(filename, Train, Point);
 *************************************/

extern const void * Train;

enum tcopmration { NAME = 0, ROUTE = 1, SPACE = 2 };

/* @param train: <Train>
 * @param person: <Person>
 * @return <Booking>
 */
void * reserve (const void * train);

void * getTrainRoute (const void * train);

void * getTrainSpace (const void * train);

void * getTrainName  (const void * train);

void * printTrainDetails (const void * train);

#endif
