#ifndef NEW_H
#define NEW_H

#include <assert.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include "Class.h"

void * interview (const void * _class, ...); 

void * new (const void * _class, ...);
void delete (void * self);

void * clone (const void * self);
int differ (const void * self, const void * b);
int metric (const void * self, const void * b, ...);
int similar (const void * self, const void *b, ...);

size_t sizeOf (const void * self);

void print (const void * self);
void store(FILE * file, const void * self);
void * extract (FILE * file, const void * class, ...);

#endif
