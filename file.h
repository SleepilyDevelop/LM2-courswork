#ifndef FILE_H
#define FILE_H

enum fmode {READ = 0, WRITE = 1, APPEND = 2, UPDATE = 3};


FILE * openFile (const char * filename, enum fmode mode);

FILE * reopenFile (const char * filename, enum fmode mode);

void closeFile (void);

void initFile (FILE * fp);

int fileEmpty (FILE * fp);

const char * getFilename (void);

FILE * getFilePointer (void);

char * scan (char * dest, int len);

#endif
