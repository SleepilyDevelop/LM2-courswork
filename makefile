SRC = main.c new.c file.c Set.c String.c Person.c Train.c Space.c Point.c Booking.c slider.c
OBJ = main.o new.o file.o Set.o String.o Person.o Train.o Space.o Point.o Booking.o slider.o -lm
APPS = application.c
APPO = application.o

compile:
	gcc -c -std=c11 $(SRC) $(APPS) && gcc $(OBJ) $(APPO)
