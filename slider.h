#ifndef SLIDER_H
#define SLIDER_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h> // link with -lm

#include "macro.h"
#include "new.h"
#include "Set.h"
#include "Train.h"
#include "Booking.h"

void * slider (const void * set);

void setNumberPerSlide(int count);

int getSelectTrainNo (void);

int count_freeElements(int no, int per, int power);

void current_slide(void);

void next_slide (void);

void prev_slide (void);

void select_train (void);

void filter_train (void);

#endif
    
