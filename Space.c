#include "Space.h"

struct Space {
    const void * class;
    int  space[3];
};

static void * Space_interview (void * _self, va_list * app)
{
    struct Space * self = _self;
    int s[3];

    printf("%20s", "train space\n");
    printf("%-20s: ", "hard level space");
    scanf("%d", & s[0]);
    printf("%-20s: ", "midl level space");
    scanf("%d", & s[1]);
    printf("%-20s: ", "easy level space");
    scanf("%d", & s[2]);

    self -> space[0] = s[0];
    self -> space[1] = s[1];
    self -> space[2] = s[2];

    getchar(); // clear i. stream from '\n'

    return _self;
}

static void * Space_ctor (void * _self, va_list * app)
{
    struct Space * self = _self;

    self -> space[HARD] = va_arg(* app, int);
    self -> space[MIDL] = va_arg(* app, int);
    self -> space[EASY] = va_arg(* app, int); 

    return self;
}

static void * Space_dtor (void * _self)
{
    return 0;
}

static void * Space_clone (const void * _self)
{
    const struct Space * self = _self;

    return new(Space, self -> space);
}

static int Space_differ (const void * _self, const void * _b)
{
    const struct Space * self = _self;
    const struct Space * b = _b;

    if (_self == _b)
        return 0;

    if (b -> class != Space)
        return 1;

    if (self -> space[0] == b -> space[0]
    && (self -> space[1] == b -> space[1])
    && (self -> space[2] == b -> space[2]))
        return 0;

    return 1;
}

static int Space_metric (const void * _self, const void * _b, va_list * app)
{
    const struct Space * self = _self;
    const struct Space * b = _b;

    int comp = va_arg(* app, int);
    
    return (self -> space[comp] < b -> space[comp]) ? -1
    :   (self -> space[comp] == b -> space[comp]) ? 0 : 1;   
}

static void * Space_print (const void * _self)
{
    const struct Space * self = _self;

    printf("[level 0: %d, level 1: %d, level 2: %d]", 
            self -> space[0], self -> space[1], self -> space[2]);

    return (void *) _self;
}

static void * Space_store (FILE * file, const void * _self)
{
    const struct Space * self = _self;

    fwrite(self -> space, sizeof(int), 3, file);

    return (void *) _self;
}

static void * Space_extract (FILE * file, const void * _class, va_list * app)
{
    const struct Class * class = _class;
    int space[3];

    fread(space, sizeof(int), 3, file);
    
    return new(_class, space[0], space[1], space[2]);
}    

static const struct Class _Space = {
    .size = sizeof(struct Space),
    .interview = Space_interview,
    .ctor = Space_ctor,
    .dtor = Space_dtor,
    .clone = Space_clone,
    .differ = Space_differ,
    .metric = Space_metric,
    .print = Space_print,
    .store = Space_store,
    .differ = Space_differ,
    .extract = Space_extract
};

const void * Space = & _Space;

// Space's functions
int getFreeSpace (const void * _space, enum stype type)
{
    const struct Space * space = _space;

    assert(space);
    return space -> space[type];
}

enum stype lower (void * _space, enum stype type)
{
	struct Space * space = _space;

	switch (type)
	{
		case HARD:
			-- (space -> space[HARD]);
			break;

		case MIDL:
			-- (space -> space[MIDL]);
			break;

		case EASY:
			-- (space -> space[EASY]);
			break;
	}
	return type;
}

const char * represent (enum stype type)
{
    switch (type)
    {
        case HARD: return "Standart";
        case MIDL: return "Standart prem.";
        case EASY: return "Business prem.";
    }
}
