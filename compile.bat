@ECHO OFF

set "SRC=main.c new.c wslider.c application.c Set.c String.c Train.c Point.c Space.c Booking.c Person.c file.c"
set "OBJ=main.o new.o wslider.o application.o Set.o String.o Train.o Point.o Space.o Booking.o Person.o file.o"

:: compiling
gcc -c -std=c11 %SRC% && gcc -o prog.exe %OBJ%

:: make clean
del *.o
