#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "file.h"

#include "new.h"
#include "Class.h"
#include "String.h"

struct String {
	const void * class; /* Должен быть первым */
	char * text;
};

static void * String_interview (void * _self, va_list * app)
{
    struct String * self = _self;
    const char * title = va_arg(* app, const char *);
    int len = va_arg(* app, int);

    self -> text = malloc(len);    

    printf("%-20s: ", title);
    scan(self -> text, len);

    return self;
}

static void * String_ctor (void * _self, va_list * app) 
{
	struct String * self = _self;
	const char * text = va_arg(* app, const char *);
	
	self -> text = malloc(strlen(text) + 1);
	assert(self -> text);
	strcpy(self -> text, text);
	return self;
}

static void * String_dtor (void * _self) 
{
	struct String * self = _self;
	
	free(self -> text), self -> text = 0;
	return 0;
}

static void * String_clone (const void * _self) 
{
	const struct String * self = _self;

	assert(self);
	return new(String, self -> text);
}

static int String_differ (const void * _self, const void * _b) 
{
	const struct String * self = _self;
	const struct String * b = _b;
	
	if (self == b)
		return 0;
	if (! b || b -> class != String)
		return 1;
	return strcmp(self -> text, b -> text);
}

static int String_metric (const void * _self, const void * _b, va_list * app)
{
    /**************************************************************************
     * RETURN VALUE                                                           *
     *     -1 if _self less than _b                                           *
     *      0 if _self equal _b                                               *
     *      1 if _self greater than _b                                        *
     *************************************************************************/
    return strcmp(text(_self), text(_b));
}

static int String_similar (const void * _self, const void * _b, va_list * app)
{
    return strstr(text(_self), text(_b)) == NULL ? 0 : 1;
} 

static void * String_print (const void * _self)
{
    const struct String * self = _self;

    assert(self);
    printf("%s", self -> text);

    return (void *) _self;
}

static void * String_store (FILE * file, const void * _self) 
{
    const struct String * self = _self;
    int _length = (int) length(self) + 1;
    
    fwrite(& _length, sizeof(int), 1, file);
    fwrite(self -> text, _length, 1, file);

    return (void *) _self;
}

static void * String_extract (FILE * file, const void * _class, va_list * app)
{
    char * text;
    int _length; 
    
    fread(& _length, sizeof(int), 1, file);

    text = malloc(_length);
    fread(text, _length, 1, file);

    return new(_class, text);
}

static const struct Class _String = {
	sizeof(struct String),
	String_interview,
	String_ctor,  String_dtor,
	String_clone, String_differ,
    String_metric,String_similar,
	String_print, String_store,
   	String_extract
};

const void * String = & _String;

// String's fuctions
const char * text (const void * _string)
{
	const struct String * string = _string;

	return string -> text;
}

size_t length (const void * _string)
{
    const struct String * string = _string;

    return strlen(string -> text);
}
