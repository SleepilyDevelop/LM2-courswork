#include "Booking.h"

// new(Booking, <Person>, <String>, <enum stype>);
struct Booking {
    const void * class; // Class
    void * person;      // Person 
    void * trname;      // String
    enum stype type;    
};

static void * Booking_interview (void * _self, va_list * app)
{
    struct Booking * self = _self;

    void * p = interview(Person);
    void * t = va_arg(* app, void *);

    int type = 0;
    printf("%-20s: ", "Enter place");
    while (scanf("%d", & type)) 
        if (type >= 0 && type <= 2)
            break;

    self -> person = p;
    self -> trname = t;
    self -> type = type;

    return _self;
}

static void * Booking_ctor (void * _self, va_list * app)
{
    struct Booking * self = _self;

    void * person = va_arg(* app, void *);
    void * trname = va_arg(* app, void *);
    int type = va_arg(* app, int);

    assert(person && trname);
    self -> person = person;
    self -> trname = trname;
    self -> type = type;

    return _self;
}

static void * Booking_dtor (void  * _self)
{
    struct Booking * self = _self;

    delete( self -> person);
    delete( self -> trname);

    return 0;
}

static void * Booking_print (const void * _self)
{
    const struct Booking * self = _self;
    
    print( self -> trname );
    putchar(' '), putchar('(');
    printf("%s", represent( self -> type ));
    putchar(')'), putchar(' ');
    print( self -> person ); 

    return (void *) _self;
}

static void * Booking_store (FILE * file, const void * _self)
{
    const struct Booking * self = _self;

    store(file, self -> person);
    store(file, self -> trname);
    fwrite(& (self -> type), sizeof(int), 1, file);

    return (void *) _self;
}

static void * Booking_extract (FILE * file, const void * _class, va_list * app)
{
    const struct Class * string = String;

    void * person = extract(file, Person);
    void * trname = extract(file, String);
    enum stype type;
    fread(& type, sizeof(int), 1, file);

    return new(_class, person, trname, type);
}
    
const struct Class _Booking = {
    .size = sizeof(struct Booking),
    .interview = Booking_interview,
    .ctor = Booking_ctor,
    .dtor = Booking_dtor,
    .print = Booking_print,
    .store = Booking_store,
    .extract = Booking_extract
};

const void * Booking = & _Booking;
