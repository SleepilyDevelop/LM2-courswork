#include "slider.h"

// Дефолтное состояние ...
static struct termios stored_termios;

// Набор выводящийся на слайды
static const void * set = 0;

// Количество записей на один слайд
static int perslide = 4;

// Текущий слайд
static int current  = 0;

// Номер выбранного поезда
static int trainno  = -1;

static int help = 0;

static int setDefaultParam (void)
{
    set = 0;
    trainno = -1;
    current = 0;

    return 1;
};

void set_keypress (void) 
{
    tcgetattr(0, & stored_termios);

    struct termios new_termios = stored_termios;
        new_termios.c_lflag &= ~(ECHO | ICANON);
        new_termios.c_cc[VMIN] = 1;  
        new_termios.c_cc[VTIME] = 0; 

    tcsetattr(0, TCSANOW, & new_termios);
}

void reset_keypress (void) 
{
    tcsetattr(0, TCSANOW, & stored_termios);
}

int  getSelectTrainNo (void)
{
    return trainno;
}

void setNumberPerSlide (int count)
{
    perslide = count;
}

void * slider (const void * _set)
{
    set_keypress();

    if (set == 0)
        set = _set;

    if (power(set) == 0) {
        puts("Error: no data for sliding");
        reset_keypress();
        setDefaultParam();
        return NULL;
    }

    int code = 0;
    do {
        if (code == 'h' || code == 'H')
            help = help == 0 ? 1 : 0;

        else if (code == '.' || code == '>')
            next_slide();

        else if (code == ',' || code == '>')
            prev_slide();

        else if (code == 'f' || code == 'F')
            filter_train();

        else if (code == 's')
        {
            select_train();
            break;
        }

        current_slide();
    } while ((code = getchar()) != 'b');

    reset_keypress();
    CLEAR_SCRIN;
    
    if (trainno == -1)
    {
        setDefaultParam();
        return NULL;
    }

    if (trainno >= power(set))
    {
        return get(set, power(set) - 1);
    }
    
    void * ret = get(set, trainno);

    setDefaultParam();

    return ret;
}

void select_train (void)
{
    reset_keypress();
    
    printf("Enter the number of the train: ");
    scanf("%d", & trainno); getchar();
}

void filter_train (void)
{
    CLEAR_SCRIN;

    putchar('\n');
    putchar('\t');
    printf("%-8s%-16s", "s.", "by station name");
    putchar('\t');
    printf("%-8s%-16s", "d.", "by dep. time");
    putchar('\n');
    putchar('\n');
    putchar('\t');
    printf("%-8s%-16s", "b.", "back");
    putchar('\n');

    int code = 0;
    while ((code = getchar()) != 'b')
    {
        if (code == 's' || code == 'S')
        {
            reset_keypress();

            void * station = interview(String, "Station name", 20); 
            void * point = new(Point, text(station), "", "");
            
            void * ret = like(set, point, ROUTE);

            if (ret != 0)
                set = ret;

            delete(station);
            delete(point);

            set_keypress();
            break;
        }   

        else if (code == 'd' || code == 'D')
        {
            reset_keypress();

            void * dep = interview(String, "Departure time", 20);
            delete(dep);

            set_keypress();
            break;
        }
    }

    CLEAR_SCRIN;
}

static void slider_instructions (void)
{
    putchar('\n');

    putchar('\t');
    printf("%-8s%-16s", "< | >", "prev | next");
    putchar('\t');
    printf("%-8s%-16s", "b.", "back");

    putchar('\n');
    putchar('\n');

    putchar('\t');
    printf("%-8s%-16s", "s.", "select");
    putchar('\t');
    printf("%-8s%-16s", "f.", "filter");

    putchar('\n');
    putchar('\n');
}

static void print_slide (int count)
{
    CLEAR_SCRIN;

    if (help != 0)
        slider_instructions();

    printf("%-59s", "to get help, press the \"h\" key");
    printf("Slide no. %d [%d/%zu]\n\n", current, (1 + current) * perslide, power(set));
    printf("%-4s%-20s%-20s%-20s%-16s\n\n","#", "Dep.", "From", "To", "Arr.");

    for (int i = 0; i < count; ++ i)
    {
        printf("%-4d", i + current * perslide);
        print( get(set, i + current * perslide) );
        putchar('\n');
        putchar('\n');
    }
}

// Calculates a number of free set elements to be slided
int count_freeElements (int no, int per, int power)
{
    int x = (1 + no) * per;
    int y = power;

    if (x < 0 || y < 0)
        return 0;

    if (x > y + per)
        return 0;

    double arg = atan( (double) y / (double) x );

    if (arg >= atan(1))
       return per;
    else
       return power % per; 
}

void current_slide (void)
{
    int count = count_freeElements(current, perslide, power(set));
    print_slide(count);
}

void next_slide (void)
{   
    ++ current;
    int count = count_freeElements(current, perslide, power(set));

    if (count == 0) {
        -- current;
        count = count_freeElements(current, perslide, power(set));
    }
    print_slide(count); 
}

void prev_slide (void)
{
    if (current != 0)
        -- current;

    int count = count_freeElements(current, perslide, power(set));
    print_slide(count);
}
