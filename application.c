#include "new.h"
#include "Class.h"
#include "slider.h"
#include "Set.h"
#include "String.h"
#include "Train.h"
#include "Point.h"
#include "Space.h"
#include "Person.h"
#include "file.h"
#include "macro.h"


int service_menu (void)
{
    printf("Service\n\n");
    printf("\ta:\t%-20s",   "register   (add)");
    printf("\td:\t%-20s\n\n", "unregister (drop)");
    
    printf("\tu:\t%-20s",   "update");
    printf("\ts:\t%-20s\n\n", "store");

    printf("\tp:\t%-20s",   "list     (print)");
    printf("\tb:\t%-20s\n\n", "bookings");

    printf("\tq:\t%-20s\n\n", "quit");
}

int booking_menu (void)
{
    printf("Booking\n\n");
    printf("\tr:\t%-20s", "Book a train ticket");
    printf("\tq:\t%-20s", "quit\n\n");
}

int common_menu (void)
{
    puts("Welcom to Anton's railway program\n");
    puts("\tfor service press the '0' key\n");
    puts("\tfor booking press the '1' key\n"); 
    puts("\tfor exit press the 'q' key\n");
}

int menu (int no)
{
    if (no == 0)
        service_menu();
    
    if (no == 1)
        booking_menu();

    if (no == 2)
        common_menu();

    printf("\n\tdo: ");
    int choose = getchar();
    
    while (getchar() != '\n')
        continue;

    CLEAR_SCRIN;

    return choose; 
}

void sapp (void)
{
    CLEAR_SCRIN;

    static void * bookings = 0;
    static void * trains = 0;   // set<Train>
    static const char * storage = "trains.dat";

    FILE * file = 0;

    file = openFile(storage, READ);
        trains = extract(file, Set, Train);
    closeFile();

    file = openFile("bookings.dat", READ);
        bookings = extract(file, Set, Booking);
    closeFile();

    int choose = 0;
    while ((choose = menu(0)) != 'q')
    {
        CLEAR_SCRIN;
        
        if (choose == 'a')
            add(trains, interview(Train));

        else if (choose == 'd')
            delete(slider(trains));

        else if (choose == 'u')
            ;

        else if (choose == 'p')
            slider(trains);

        else if (choose == 'b')
            print(bookings), getchar();

        else if (choose == 's')
        {
            file = openFile(storage, WRITE);
                store(file, trains);
            closeFile();
        }
    }

    delete(trains);

    CLEAR_SCRIN;
}

void capp (void)
{
    CLEAR_SCRIN;

    FILE * fp;

    // Набор поездов 
    fp = openFile("trains.dat", READ);
        void * trains = extract(fp, Set, Train);
    closeFile();
    
    // Количество поездов
    const size_t n = power(trains);

    // набор брони для поездов
    fp = openFile("bookings.dat", READ);
        void * bookings = extract(fp, Set, Booking);
    closeFile();

    // Указатель для работы с выбранным нвбором брони
    void * current = 0;

    // Указатель для работы с выбранной бронью
    void * booking = 0;

    // Указатель для работы с выбраным поездом
    void * train   = 0;

    int choose = 0;
    while ((choose = menu(1)) != 'q')
    {
        int trainno = 0;
         
        train   = slider(trains); 
        if (train == NULL) 
		continue;
        trainno = getSelectTrainNo();
        booking = reserve(train);
        
        add(bookings, booking); 
    }

    fp = openFile("bookings.dat", WRITE);
        store(fp, bookings);

    fp = reopenFile("trains.dat", WRITE);
        store(fp, trains);

    closeFile();
}

void application (void)
{
    CLEAR_SCRIN;

    int choose = 0;
    while ((choose = menu(2)) != 'q')
    {
        if (choose == '0')
            sapp();

        else if (choose == '1')
            capp();
    }
            
}
