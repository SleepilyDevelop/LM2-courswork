#include <stdio.h>
#include <stdlib.h>

#include "slider.h"
#include "file.h"
#include "new.h"

#include "Set.h"
#include "String.h"
#include "Person.h"
#include "Train.h"
#include "Point.h"
#include "Space.h"

#include "application.h"

#define FILE_TRAINS 	"trains.dat"
#define FILE_BOOKINGS 	"bookings.dat"

int main (int argc, char * argv[])
{
    setNumberPerSlide(8);
    application();

    return 0;
}
