#include "slider.h"

// Набор выводящийся на слайды
static const void * set = 0;

// Количество записей на один слайд
static int perslide = 4;

// Текущий слайд
static int current  = 0;

// Номер выбранного поезда
static int trainno  = -1;

static int help = 0;

static int setDefaultParam (void)
{
    set = 0;
    trainno = -1;
    current = 0;

    return 1;
};

int  getSelectTrainNo (void)
{
    return trainno;
}

void setNumberPerSlide (int count)
{
    perslide = count;
}

void * slider (const void * _set)
{
    if (set == 0)
        set = _set;

    if (power(set) == 0) {
        puts("Error: no data for sliding");
        setDefaultParam();
        return NULL;
    }

    int code = 0;
    do {
        if (code == 'h' || code == 'H')
            help = help == 0 ? 1 : 0;

        else if (code == '.' || code == '>')
            next_slide();

        else if (code == ',' || code == '>')
            prev_slide();

        else if (code == 'f' || code == 'F')
            filter_train();

        else if (code == 's')
        {
            select_train();
            break;
        }

        current_slide();
    } while ((code = getchar()) != 'b');
	getchar();
    CLEAR_SCRIN;
    
    if (trainno == -1)
    {
        setDefaultParam();
        return NULL;
    }

    if (trainno >= power(set))
    {
        return get(set, power(set) - 1);
    }
    
    void * ret = get(set, trainno);

    setDefaultParam();

    return ret;
}

void select_train (void)
{
    printf("Enter the number of the train: ");
    scanf("%d", & trainno); getchar();
}

void filter_train (void)
{
    CLEAR_SCRIN;

    putchar('\n');
    putchar('\t');
    printf("%-8s%-16s", "s.", "by station name");
    putchar('\t');
    printf("%-8s%-16s", "d.", "by dep. time");
    putchar('\n');
    putchar('\n');
    putchar('\t');
    printf("%-8s%-16s", "b.", "back");
    putchar('\n');

    int code = 0;
    while ((code = getchar()) != 'b')
    {
        if (code == 's' || code == 'S')
        {
            void * station = interview(String, "Station name", 20); 
            void * point = new(Point, text(station), "", "");
            
            void * ret = sub(set, point, ROUTE);

            if (ret != 0)
                set = ret;

            delete(station);
            delete(point);
            break;
        }   

        else if (code == 'd' || code == 'D')
        {
            void * dep = interview(String, "Departure time", 20);
            delete(dep);
            break;
        }
    }

    CLEAR_SCRIN;
}

static void slider_instructions (void)
{
    putchar('\n');

    putchar('\t');
    printf("%-8s%-16s", "< | >", "prev | next");
    putchar('\t');
    printf("%-8s%-16s", "b.", "back");

    putchar('\n');
    putchar('\n');

    putchar('\t');
    printf("%-8s%-16s", "s.", "select");
    putchar('\t');
    printf("%-8s%-16s", "f.", "filter");

    putchar('\n');
    putchar('\n');
}

static void print_slide (int count)
{
    CLEAR_SCRIN;

    if (help != 0)
        slider_instructions();

    printf("%-59s", "to get help, press the \"h\" key");
    printf("Slide no. %d [%d/%zu]\n\n", 
		current, (1 + current) * perslide, power(set));
    printf("%-4s%-20s%-20s%-20s%-16s\n\n","#", "Dep.", "From", "To", "Arr.");

    for (int i = 0; i < count; ++ i)
    {
        printf("%-4d", i + current * perslide);
        print( get(set, i + current * perslide) );
        putchar('\n');
        putchar('\n');
    }
}

// Calculates a number of free set elements to be slided
int items (int no, int per, int power)
{
    int x = (1 + no) * per;
    int y = power;

    if (x < 0 || y < 0)
        return 0;

    if (x > y + per)
        return 0;

    double arg = atan( (double) y / (double) x );

    if (arg >= atan(1))
       return per;
    else
       return power % per; 
}

void current_slide (void)
{
    int count = items(current, perslide, power(set));
    print_slide(count);
}

void next_slide (void)
{   
    ++ current;
    int count = items(current, perslide, power(set));

    if (count == 0) {
        -- current;
        count = items(current, perslide, power(set));
    }
    print_slide(count); 
}

void prev_slide (void)
{
    if (current != 0)
        -- current;

    int count = items(current, perslide, power(set));
    print_slide(count);
}

