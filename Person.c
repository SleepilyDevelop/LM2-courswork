#include "Person.h"

// new(Person, 
struct Person {
	const void * class;
    void * name;        // <String>
    int age;
};

static void * Person_interview (void * _self, va_list * app)
{
    struct Person * self = _self;
    
    self -> name = interview(String, "Your full name", 20);
    
    printf("%-20s: ", "Your age");
    scanf("%d", & self -> age);
    
    getchar(); // clear input stream from '\n'

    return _self;
}

static void * Person_ctor (void * _self, va_list * app)
{
    struct Person * self = _self;

    const char * name = va_arg(* app, const char *);
    int age = va_arg(* app, int);
    
    assert(name);
    self -> name = new(String, name);
    self -> age  = age;

    return _self;
}

static void * Person_dtor (void * _self)
{
    struct Person * self = _self;
    
    delete(self -> name), self -> name = 0;
}

static void * Person_clone (const void * _self)
{
    const struct Person * self = _self;
    char * name = malloc( length(self -> name) + 1);
    
    strcpy(name, text(self -> name));

    return (void *) new(Person, name, self -> age); 
}

static void * Person_print (const void * _self)
{
    const struct Person * self = _self;

    print(self -> name);
    printf(" %d year old", self -> age);

    return (void *) _self;
}

static void * Person_store (FILE * file, const void * _self)
{
    const struct Person * self = _self;

    store(file, self -> name);
    fwrite(& self -> age, sizeof(int), 1, file);

    return (void *) _self;
}

static void * Person_extract (FILE * file, const void * _class, va_list * app)
{
    const struct Class * string = String;

    void * n = extract(file, String);
    char * name = malloc(length(n) + 1);
    strcpy(name, text(n)), delete(n);

    int age = 0;
    fread(& age, sizeof(int), 1, file);

    void * self = new(_class, name, age);

    return self;
}

static struct Class _Person = {
    .interview = Person_interview,
    .size = sizeof(struct Person),
    .ctor = Person_ctor,
    .dtor = Person_dtor,
    .clone = Person_clone,
    .print = Person_print,
    .store = Person_store,
    .extract = Person_extract
};

const void * Person = & _Person;

// Person's functions
const char * getPersonName (const void * person)
{
    const struct Person * p = person;

    assert(p && p -> name);
    return text(p -> name);
}

int getPersonAge (const void * person)
{
    const struct Person * p = person;

    assert(p);
    return p -> age;
}
