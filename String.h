#ifndef STRING_H
#define STRING_H

// new(String, "Text");
extern const void * String;

const char * text (const void * string);

size_t length (const void * string);

#endif
