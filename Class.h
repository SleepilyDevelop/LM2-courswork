#ifndef CLASS_T
#define CLASS_T

#include <stdlib.h>
#include <stdarg.h>

struct Class {
	size_t size;
	void * (* interview) (void * self, va_list * app);
	void * (* ctor) (void * self, va_list * app);
	void * (* dtor) (void * self);
	void * (* clone) (const void * self);
	int (* differ) (const void * self, const void * b);
	int (* metric) (const void * self, const void * b, va_list * app); 
	int (* similar) (const void * Self, const void * b, va_list * app);
	void * (* print) (const void * self);
	void * (* store) (FILE * file, const void * sefl);
	void * (* extract) (FILE * file, const void * class, va_list * app);
};

#endif
