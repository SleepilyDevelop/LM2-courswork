#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "file.h"

static enum fmode mode;

static FILE * file = 0;
static char * filename = 0;

FILE * openFile (const char * _filename, enum fmode _mode)
{
	if (file == 0 && filename == 0) {
		size_t length = strlen(_filename) + 1;

		filename = malloc(length);
		strcpy(filename, _filename);

		mode = _mode;

		if (mode == READ)
			assert((file = fopen(filename, "rb")) != NULL);

		else if (mode == WRITE)
			assert((file = fopen(filename, "wb")) != NULL);

        	else if (mode == APPEND)
        		assert((file = fopen(filename, "a+b")) != NULL);

        	else if (mode == UPDATE)
        		assert((file = fopen(filename, "r+b")) != NULL);
	}
	return file;	
}

FILE * reopenFile (const char * _filename, enum fmode _mode)
{
	if (file == 0)
		return openFile(_filename, _mode);
	
	closeFile();
	return openFile(_filename, _mode);
}

void closeFile (void)
{
	if (file != 0) {
		assert(fclose(file) == 0);
		free(filename), filename = 0;
		file = 0;
	}
}

void initFile (FILE * fp)
{
	int init = 0;
	fwrite(& init, sizeof(int), 1, fp);
}

int fileEmpty (FILE * fp)
{
	int pos = 0;
    
	fseek(fp, 1L, SEEK_CUR);

	pos = ftell(fp);

	printf("%i", pos);
	

	if (pos != 0)
		return 0;
	else
		return 1;
    
}

FILE * getFilePointer (void)
{
	return file;
}

const char * getFilename (void)
{
	return (const char *) filename;
}



// stdin's functions
char * scan (char * dest, int len)
{
    char * ret = fgets(dest, len, stdin);

    char * p = dest;
    while (* (p ++) != '\0')
        if (* p == '\n')
            break;

    if (* p != '\n')
        while (getchar() != '\n')
            continue;
    else 
        * p = '\0';

    return ret;
}
    
