/******************************************************************************
 * 
 * Booking a train ticket
 *
 * new(Booking, <String>, <String>
 *
 *****************************************************************************/

#ifndef BOOKING_H
#define BOOKING_H

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>

#include "new.h"
#include "Class.h"
#include "String.h"
#include "Person.h"
#include "Train.h"
#include "Space.h"

extern const void * Booking;

#endif
