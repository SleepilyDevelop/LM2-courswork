#ifndef MACRO_H
#define MACRO_H

#define SYS_COMPILE 1 // 0 - linux, 1 - windows

#if SYS_COMPILE == 0
    
    #define CLEAR_SCRIN system("clear")

#endif

#if SYS_COMPILE == 1

    #define CLEAR_SCRIN system("cls");

#endif

#endif
