#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "file.h"
#include "new.h"
#include "Class.h"
#include "Set.h"

static const size_t defsiz = 4;

struct Set {
   const void * class;
   const void * type;
   void * heap;
   int * order;
   size_t size;
   size_t count;
   size_t lim;
};

static void * Set_interview (void * _self, va_list * app)
{
    struct Set * self = _self;
    const struct Class * class = va_arg(* app, const void *);

    int count = 0;
    printf("%-20s: ", "Number");
    scanf("%d", & count); getchar(); // claer i. stream from '\n'
    
    self -> type  = class;
    self -> size  = class -> size;
    self -> heap  = 0;
    self -> order = 0;
    self -> count = 0;
    self -> lim   = defsiz;

    for (int i = 0; i < count; ++ i)
        add(self, interview(class));
        
    return _self;
}

static void * Set_ctor (void * _self, va_list * app)
{
    const struct Class * cp = va_arg(* app, const void *); 
    struct Set * self = _self;

    self -> type  = cp;
    self -> size  = cp-> size;
    self -> heap  = 0;
    self -> order = 0;
    self -> count = 0;
    self -> lim   = defsiz;

    return self;
}

static void * Set_dtor (void * _self)
{
    struct Set * self = _self;
    void * element = 0;
    
    if (self -> heap == 0)
        return 0;

    for (int i = 0; i < self -> count; ++ i)
        delete( get(_self, i) );
    free(self -> heap), self -> heap = 0;

    if (self -> order != 0)
        free(self -> order), self -> order = 0;
    
    return 0;
}

static void * Set_clone (const void * _self)
{
    const struct Set * self = _self;
    const struct Class * const ** type = self -> heap;

    void * clone = new(self -> class, ** type);

    for (int i = 0; i < self -> count; ++ i)
        add(clone, get(_self, i));

    return clone;
}

static int Set_metric (const void * _self, const void * _b, va_list * app)
{
    if ( power(_self) == power(_b) )
        return  0;

    if ( power(_self) < power(_b) )
        return -1;
    
    if ( power(_self) > power(_b) )
        return  1;
}

static void * Set_print (const void * _self)
{
    const struct Set * self = _self;
    const void * element = 0;

    for (int i = 0; i < self -> count; ++ i)
    {
        element = get(_self, i);
        
        printf("%d - ", i);
        print(element); 
        putchar('\n');
    }

    return (void *) _self;
}

static void * Set_store (FILE * file, const void * _self)
{
    const struct Set * self = _self;
    int _count = (int) power(self); 
    
    fwrite(& _count, sizeof(int), 1, file);

    for (int i = 0; i < _count; ++ i)
    {
        const struct Class ** el = get(_self, i);

        assert(el && (* el) -> store);
        (* el) -> store(file, el);
    }

    return (void *) _self;
}

static void * Set_extract (FILE * file, const void * _class, va_list * app)
{
    const struct Class * class = _class;
    const struct Class * sub = va_arg(* app, const void *);

    struct Set * set = new(_class, sub);

    int _count = 0;
    fread(& _count, sizeof(int), 1, file);

    for (int i = 0; i < _count; ++ i)
        add(set, sub -> extract(file, (const void *) sub, app));

    return (void *) set;
}

static const struct Class _Set = {
    .size = sizeof(struct Set),
    .interview = Set_interview,
    .ctor = Set_ctor,
    .dtor = Set_dtor,
    .clone = Set_clone,
    .metric = Set_metric,
    .print = Set_print,
    .store = Set_store,
    .extract = Set_extract
};

const void * Set = & _Set;



/*** Set's functions ***/
void * add (void * _set, void * _element)
{
    struct Set * set = _set;
    const struct Class * const * element = _element;
    const struct Class * const ** heap;

    size_t size = sizeof(void *);
    size_t shift = size * set -> count;
    
    if (set -> count == 0)
        set -> heap = calloc(set -> lim, size);
    
    else if (set -> count == set -> lim)
    {
        void * buff = set -> heap;
         
        set -> lim *= 2;
        set -> heap = calloc(set -> lim, size);
        memcpy(set -> heap, buff, size * set -> count);

        memset(buff, 0, size * set -> count);
        free(buff);
    }

    heap = set -> heap + shift;
    * heap = element; 

    ++ set -> count;

    return (void *) * heap;
}

void * drop (void * _set, void * _element)
{
    struct Set * set = _set;
    const struct Class * const ** heap;
    
    size_t size = sizeof(void *),
           n = index(_set, _element),
           b = n * size,
           last = set -> count - 1,
           copy = set -> count - (n + 1);

    heap = set -> heap + (last * size); 
    memmove(set -> heap + b, set -> heap + (b + size), copy * size);
    memset(set -> heap + (last * size), 0, size);

    -- set -> count;

    return _element;
}

void * find (const void * _set, const void * _element)
{
    const struct Set * set = _set;
    const struct Class * const * element = _element;
    const struct Class * const ** heap;

    size_t size  = sizeof(void *);
    size_t shift = 0;

    for (int i = 0; i < set -> count; ++ i)
    {
        shift = size * i;
        heap  = (set -> heap) + shift;
        
        if (* heap == element)
            return (void *) element;  
    }

    return 0;
}

void * get (const void * _set, size_t index)
{
    const struct Set * set = _set;
    const struct Class * const ** element = 0;

    assert(set);
    if (index >= set -> count)
        return 0;
    
    size_t size  = sizeof(void *);
    size_t shift = size * index;
    if (set -> order != 0)
        shift = size * set -> order[index];

    if (index < set -> count && index >= 0)
        element = set -> heap + shift;

    return (void *) * element;
}

size_t index (const void * _set, const void * _element)
{
    const struct Set * set = _set;
    const struct Class * const * element = _element;
    const struct Class * const ** heap;

    size_t size = sizeof(void *);
    size_t shift = 0;

    for (int i = 0; i < set -> count; ++ i)
    {
        shift = size * i;
        heap = (set -> heap) + shift;
        
        if (* heap == element)
            return i; 
    }

    return -1;
}

size_t power (const void * _set)
{
    const struct Set * set = _set;

    assert(set);
    return set -> count;
}

int consists (const void * _set, const void * _element)
{
    return find(_set, _element) != 0;
} 

void * sub (const void * _set, const void * _element, ...)
{
    
    const struct Set * set = _set;

    const struct Class * class = set -> type;
    struct Set * ret = new(Set, class);  
    
    va_list ap;
    for (int i = 0; i < power(set); ++ i)
    {
        void * el = 0;

        el = get(set, i);
        va_start(ap, _element);
        if (class -> similar(el, _element, & ap))
            add(ret, el);
        va_end(ap);
    }

    if (power(ret) == 0)
        return 0;

    return ret;
}

// not completed
void * sort (void * _set, ...) 
{
    struct Set * set = _set;

    if (set -> heap != 0 && set -> order == 0)
        set -> order = calloc(sizeof(int), set -> lim);

    for (int i = 0; i < set -> count; ++ i)
        set -> order[i] = i;
    
    va_list ap;
    for (int i = 0; i < set -> count - 1; ++ i)
        for (int j = i + 1; j < set -> count; ++ j)
        {
            va_start(ap, _set);
            if (metric(get(_set, i), get(_set, j), & ap) > 0)
            {
                int tmp = set -> order[i];
                set -> order[i] = set -> order[j];
                set -> order[j] = tmp;
            }    
            va_end(ap);
        }

    return _set;
}
