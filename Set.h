#ifndef SET_H
#define SET_H

// new(Set, TypeName);
extern const void * Set;

void * add (void * set, void * element);

void * drop (void * set, void * element);

void * find (const void * _set, const void * element);

void * get (const void * _set, size_t index);

size_t index (const void * set, const void * element);

size_t power (const void * set);

int consists (const void * set, const void * element);

void * sub (const void * set, const void * element, ...);

void * sort (void * set, ...); 

#endif
