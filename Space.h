#ifndef SPACE_H
#define SPACE_H

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>

#include "Class.h"
#include "new.h"

extern const void * Space;

enum stype {HARD = 0, MIDL = 1, EASY = 2};

int getFreeSpace (const void * space, enum stype type);

const char * represent (enum stype type);

enum stype lower (void * space, enum stype type);

#endif
