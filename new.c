#include "new.h"

void * interview (const void * _class, ...)
{
    const struct Class * class = _class;
    void * p = calloc(1, class -> size);

    assert(p);
    * (const struct Class **) p = class;
    
    if (class -> interview) {
        va_list ap;

        va_start(ap, _class);
        p = class -> interview(p, & ap);
        va_end(ap);
    }
    return p;
}

void * new (const void * _class, ...)
{
	const struct Class * class = _class;
	void * p = calloc(1, class -> size);
	
	assert(p);
	* (const struct Class **) p = class;
	
	if (class -> ctor) {
		va_list ap;
		
		va_start(ap, _class);
		p = class -> ctor(p, & ap);
		va_end(ap);
	}
	return p;
}

void delete (void * self)
{
	const struct Class ** cp = self;
	
	if (self && * cp && (* cp) -> dtor)
		self = (* cp) -> dtor(self);

	free(self), self = 0;
}

void * clone (const void * self)
{
	const struct Class * const  * cp = self;
	
	assert(self && * cp && (* cp) -> clone);
	return (* cp) -> clone(self);
}

int differ (const void * self, const void * b)
{
	const struct Class * const * cp = self;
	
	assert(self && * cp && (* cp) -> differ);
	return (* cp) -> differ(self, b);
}

int metric (const void * self, const void * b, ...)
{
    const struct Class * const * cp = self;
    const struct Class * const * bcp = b;
    int ret = 0;
    
    assert((* cp) == (* bcp));
    assert(self && * cp && (* cp) -> metric);

    va_list ap;

    va_start(ap, b);
    ret = (* cp) -> metric(self, b, & ap);
    va_end(ap);

    return ret; 
}

int similar (const void * self, const void * b, ...)
{
    const struct Class * const * cp = self;
    const struct Class * const * bcp = b;
    int ret = 0;

    assert(cp && bcp && (* cp) -> similar);
    va_list ap;

    va_start(ap, b);
    ret = (* cp) -> similar(self, b, & ap);
    va_end(ap);

    return ret;
}

size_t sizeOf (const void * self)
{
	const struct Class * const * cp = self;
	
	assert(self && * cp);
	return (* cp) -> size;
}

void print (const void * self)
{
	const struct Class * const * cp = self;
	
	assert(self && * cp && (* cp) -> print);
	(* cp) -> print(self);
}

void store (FILE * file, const void * self)
{
	const struct Class * const * cp = self;

	assert(self && * cp && (* cp) -> store);
	(* cp) -> store(file, self);
}

void * extract (FILE * file, const void * _class, ...)
{
    const struct Class * class = _class;

    assert(class);
    void * result;

    if (class -> extract) 
    {
        va_list ap;
        
        va_start(ap, _class);
        result = class -> extract(file, _class, & ap);
        va_end(ap);
    }

    return result;
}
