#ifndef POINT_H
#define POINT_H

extern const void * Point;

enum pcomparasion {STAT = 0, DEP = 1, ARR = 2};

const char * getStationName (const void * point);

const char * getArrivalTime (const void * point);

const char * getDepartureTime (const void * point);

#endif
